import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {HomeComponent} from './home/home.component';
import {LandingComponent} from './landing/landing.component';
import {NgxsModule} from '@ngxs/store';
import {NgxsLoggerPluginModule} from '@ngxs/logger-plugin';
import {environment} from '../environments/environment';
import {NgxsStoragePluginModule} from '@ngxs/storage-plugin';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {UserStateModule} from './states/user/user-state.module';
import {UsersModule} from './users/users.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {AuthStateModule} from './states/auth/auth-state.module';
import {NgxsRouterPluginModule} from '@ngxs/router-plugin';
import {NetworkStateModule} from './states/network/network-state.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LandingComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AuthStateModule,
    NetworkStateModule,
    UsersModule,
    AppRoutingModule,
    CoreModule,
    UserStateModule,
    NgxsModule.forRoot(),
    NgxsRouterPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot({collapsed: false, logger: console, disabled: environment.production}),
    NgxsStoragePluginModule.forRoot({key: ['auth', 'users']}),
    NgxsReduxDevtoolsPluginModule.forRoot({name: 'ng-exercise', disabled: environment.production})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
