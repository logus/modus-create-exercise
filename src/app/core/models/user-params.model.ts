interface GenericParams {
  [key: string]: string;
}

export interface UsersParams extends GenericParams {
  results: string;
  page: string;
}
