import {RandomUser} from './random-user.model';

export interface RandomUsersResponse {
  results: Array<RandomUser>;
  info: {
    seed: string;
    results: number;
    page: number;
    version: string;
  };
}
