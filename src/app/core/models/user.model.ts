export interface User {
  id?: string;
  picture?: {
    large: string;
    medium: string;
    thumbnail: string;
  };
  firstName?: string;
  lastName?: string;
  email?: string;
  phone?: string;
}





