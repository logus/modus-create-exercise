import {Component} from '@angular/core';
import {User} from '../../models/user.model';
import {Observable} from 'rxjs/internal/Observable';
import {AppStateModel} from '../../../app-state.model';
import {Store} from '@ngxs/store';
import {Login, Logout} from '../../../states/auth/auth.actions';

@Component({
  selector: 'app-content',
  templateUrl: './app-content.component.html',
  styleUrls: ['./app-content.component.scss']
})
export class AppContentComponent {
  user: User = {
    firstName: 'Ahsan',
    lastName: 'Ayaz'
  };
  isLoggedIn$: Observable<boolean> = this.store.select((state: AppStateModel) => state.auth.isLoggedIn);

  constructor(private store: Store) {
  }

  /**
   * @author Ahsan Ayaz
   * @desc Logs the users in
   */
  login() {
    this.store.dispatch(new Login());
  }

  /**
   * @author Ahsan Ayaz
   * @desc Logs the users out
   */
  logout() {
    this.store.dispatch(new Logout());
  }

}
