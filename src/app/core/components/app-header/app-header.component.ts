import {Component} from '@angular/core';
import {User} from '../../models/user.model';
import {Store} from '@ngxs/store';
import {AppStateModel} from '../../../app-state.model';
import {Observable} from 'rxjs/internal/Observable';
import {Login, Logout} from '../../../states/auth/auth.actions';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent {
  user: User = {
    firstName: 'Ahsan',
    lastName: 'Ayaz'
  };
  connected$: Observable<boolean> = this.store.select((state: AppStateModel) => state.network.connected);
  isLoggedIn$: Observable<boolean> = this.store.select((state: AppStateModel) => state.auth.isLoggedIn);

  constructor(private store: Store) {
  }

  /**
   * @author Ahsan Ayaz
   * @desc Logs the users in
   */
  login() {
    this.store.dispatch(new Login());
  }

  /**
   * @author Ahsan Ayaz
   * @desc Logs the users in
   */
  signup() {
    this.store.dispatch(new Login());
  }

  /**
   * @author Ahsan Ayaz
   * @desc Logs the users out
   */
  logout() {
    this.store.dispatch(new Logout());
  }

}
