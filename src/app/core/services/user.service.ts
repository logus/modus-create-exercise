import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {USER} from '../constants/user.constants';
import {UsersParams} from '../models/user-params.model';
import {RandomUsersResponse} from '../models/random-user-response.model';
import {Observable} from 'rxjs/internal/Observable';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  /**
   * Get random users
   */
  getRandomUsers(params: UsersParams): Observable<RandomUsersResponse> {
    return this.http.get<RandomUsersResponse>(USER.RANDOM_USERS_API, {
      params: new HttpParams({fromObject: params})
    });
  }
}
