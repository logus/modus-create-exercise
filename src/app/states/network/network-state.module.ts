import {NgModule} from '@angular/core';
import {NgxsModule} from '@ngxs/store';
import {NetworkState} from './network.state';

@NgModule({
  imports: [
    NgxsModule.forFeature([NetworkState])
  ]
})

export class NetworkStateModule {

}
