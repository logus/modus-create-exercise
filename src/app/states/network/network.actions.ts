/**
 * Commands
 */

export class SubscribeNetworkEvents {
  static type = '[Network] Subscribe network events';
}

/**
 * Events
 */

export class Online {
  static type = '[Network] Status changed to online';
}

export class Offline {
  static type = '[Network] Status changed to offline';
}
