import {Action, State, StateContext} from '@ngxs/store';
import {NetworkStateModel} from './network-state.model';
import {Offline, Online, SubscribeNetworkEvents} from './network.actions';

@State<NetworkStateModel>({
  name: 'network',
  defaults: {
    connected: true
  }
})

export class NetworkState {

  /**
   * Commands
   */

  @Action(SubscribeNetworkEvents)
  subscribeNetworkEvents(ctx: StateContext<NetworkStateModel>) {
    ctx.patchState({connected: navigator.onLine});
    window.addEventListener('online', () => ctx.dispatch(new Online()));
    window.addEventListener('offline', () => ctx.dispatch(new Offline()));
  }

  /**
   * Events
   */

  @Action(Online)
  online(ctx: StateContext<NetworkStateModel>) {
    ctx.patchState({connected: true});
  }

  @Action(Offline)
  offline(ctx: StateContext<NetworkStateModel>) {
    ctx.patchState({connected: false});
  }

}
