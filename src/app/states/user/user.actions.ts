/**
 * Commands
 */
import {User} from '../../core/models/user.model';

export class GetUsers {
  static type = '[User] Get users';
}

export class SetGetUsersBusy {
  static type = '[User] Set get users busy';

  constructor(public busy: boolean) {
  }
}

/**
 * Events
 */
export class GetUsersSuccess {
  static type = '[User] Get users success';

  constructor(public users: Array<User>) {
  }
}

export class GetUsersFailed {
  static type = '[User] Get users failed';

  constructor(public error: any) {
  }

}
