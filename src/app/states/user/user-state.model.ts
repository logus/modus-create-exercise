import {User} from '../../core/models/user.model';
import {UsersParams} from '../../core/models/user-params.model';

export interface UserStateModel {
  busy?: {
    get: boolean;
  };
  users?: Array<User>;
  params?: UsersParams;
  errors?: Array<any>;
}
