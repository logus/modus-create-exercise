import {NgModule} from '@angular/core';
import {NgxsModule} from '@ngxs/store';
import {UserState} from './user.state';
import {UserService} from '../../core/services/user.service';

@NgModule({
  imports: [
    NgxsModule.forFeature([UserState])
  ],
  providers: [
    UserService
  ]
})

export class UserStateModule {

}
