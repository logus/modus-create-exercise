import {Action, Selector, State, StateContext} from '@ngxs/store';
import {UserStateModel} from './user-state.model';
import {UserService} from '../../core/services/user.service';
import {GetUsers, GetUsersFailed, GetUsersSuccess, SetGetUsersBusy} from './user.actions';
import {RandomUsersResponse} from '../../core/models/random-user-response.model';
import {RandomUser} from '../../core/models/random-user.model';
import {User} from '../../core/models/user.model';
import {finalize} from 'rxjs/operators';
import {LogoutSuccess} from '../auth/auth.actions';

@State<UserStateModel>({
  name: 'users',
  defaults: {
    busy: {
      get: false
    },
    users: [],
    params: {
      results: '20',
      page: '1'
    },
    errors: []
  }
})

export class UserState {

  constructor(private userService: UserService) {
  }

  /**
   * Map a random user to an app user model
   */
  static mapRandomUser(randomUser: RandomUser): User {
    return {
      id: randomUser.id.value || Math.floor(Math.random() * 9999).toString(),
      firstName: randomUser.name.first,
      lastName: randomUser.name.last,
      phone: randomUser.phone,
      email: randomUser.email,
      picture: randomUser.picture
    };
  }

  /**
   * Selectors
   */

  @Selector()
  static user(state: UserStateModel) {
    return (userId: string) => {
      return state.users.find((user: User) => user.id === userId);
    };
  }

  /**
   * Commands
   */
  @Action(GetUsers)
  getUsers(ctx: StateContext<UserStateModel>) {
    const state = ctx.getState();
    if (!state.busy.get && !state.users.length) {
      ctx.dispatch(new SetGetUsersBusy(true));
      this.userService.getRandomUsers(state.params)
        .pipe(finalize(() => ctx.dispatch(new SetGetUsersBusy(false))))
        .subscribe(
          (res: RandomUsersResponse) => ctx.dispatch(new GetUsersSuccess(res.results.map(ru => UserState.mapRandomUser(ru)))),
          error => ctx.dispatch(new GetUsersFailed(error))
        );
    }
  }

  @Action(SetGetUsersBusy)
  setGetUsersBusy(ctx: StateContext<UserStateModel>, action: SetGetUsersBusy) {
    ctx.patchState({busy: {...ctx.getState().busy, get: action.busy}});
  }

  /**
   * Events
   */
  @Action(GetUsersSuccess)
  getUsersSuccess(ctx: StateContext<UserStateModel>, action: GetUsersSuccess) {
    ctx.patchState({users: action.users});
  }

  @Action(GetUsersFailed)
  getUsersFailed(ctx: StateContext<UserStateModel>, action: GetUsersFailed) {
    ctx.patchState({errors: [...ctx.getState().errors, action.error]});
  }

  @Action(LogoutSuccess)
  logoutSuccess(ctx: StateContext<UserStateModel>) {
    ctx.patchState({users: []});
  }

}
