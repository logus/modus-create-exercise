/**
 * Commands
 */

export class Login {
  static type = '[Auth] Login';
}

export class Logout {
  static type = '[Auth] Logout';
}

/**
 * Events
 */

export class LoginSuccess {
  static type = '[Auth] Login success';
}

export class LogoutSuccess {
  static type = '[Auth] Logout success';
}
