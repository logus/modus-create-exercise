import {Action, State, StateContext} from '@ngxs/store';
import {AuthStateModel} from './auth-state.model';
import {Login, LoginSuccess, Logout, LogoutSuccess} from './auth.actions';
import {Navigate} from '@ngxs/router-plugin';

@State<AuthStateModel>({
  name: 'auth',
  defaults: {
    isLoggedIn: false
  }
})

export class AuthState {

  /**
   * Commands
   */

  @Action(Login)
  login(ctx: StateContext<AuthStateModel>) {
    ctx.patchState({isLoggedIn: true});
    ctx.dispatch([new Navigate(['/home']), new LoginSuccess()]);
  }

  @Action(Logout)
  logout(ctx: StateContext<AuthStateModel>) {
    ctx.patchState({isLoggedIn: false});
    ctx.dispatch([new Navigate(['/home']), new LogoutSuccess()]);
  }

}
