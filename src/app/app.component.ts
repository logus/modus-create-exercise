import {Component, OnInit} from '@angular/core';
import {fadeAnimation} from './app-animations';
import {Store} from '@ngxs/store';
import {SubscribeNetworkEvents} from './states/network/network.actions';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})
export class AppComponent implements OnInit {
  constructor(private store: Store) {

  }

  ngOnInit() {
    this.subscribeNetworkEvents();
  }

  subscribeNetworkEvents() {
    this.store.dispatch(new SubscribeNetworkEvents());
  }

}
