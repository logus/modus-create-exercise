import {UserStateModel} from './states/user/user-state.model';
import {AuthStateModel} from './states/auth/auth-state.model';
import {NetworkStateModel} from './states/network/network-state.model';

export interface AppStateModel {
  auth: AuthStateModel;
  users: UserStateModel;
  network: NetworkStateModel;
}
