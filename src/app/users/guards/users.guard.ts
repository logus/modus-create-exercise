import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/internal/Observable';
import {Store} from '@ngxs/store';
import {AppStateModel} from '../../app-state.model';
import {Navigate} from '@ngxs/router-plugin';

@Injectable({
  providedIn: 'root'
})
export class UsersGuard implements CanActivate {

  constructor(private store: Store) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const isLoggedIn = this.store.selectSnapshot((s: AppStateModel) => s.auth.isLoggedIn);
    if (!isLoggedIn) {
      this.store.dispatch(new Navigate(['/home']));
    }
    return isLoggedIn;
  }
}
