import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/internal/Observable';
import {User} from '../../core/models/user.model';
import {Store} from '@ngxs/store';
import {UserState} from '../../states/user/user.state';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  user$: Observable<User>;

  constructor(private route: ActivatedRoute,
              private store: Store) {
  }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.user$ = this.store.select(UserState.user).pipe(map(getById => getById(this.route.snapshot.paramMap.get('id'))));
  }


}
