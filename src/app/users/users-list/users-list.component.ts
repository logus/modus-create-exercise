import {Component, OnInit} from '@angular/core';
import {Actions, ofActionDispatched, Store} from '@ngxs/store';
import {AppStateModel} from '../../app-state.model';
import {GetUsers} from '../../states/user/user.actions';
import {Observable} from 'rxjs/internal/Observable';
import {User} from '../../core/models/user.model';
import {Online} from '../../states/network/network.actions';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  users$: Observable<Array<User>> = this.store.select((state: AppStateModel) => state.users.users);
  connected$: Observable<boolean> = this.store.select((state: AppStateModel) => state.network.connected);
  loadingUsers$: Observable<boolean> = this.store.select((state: AppStateModel) => state.users.busy.get);

  constructor(private store: Store,
              private actions: Actions) {
  }

  ngOnInit() {
    this.subscribeEvents();
    this.getUsers();
  }

  subscribeEvents() {
    this.actions.pipe(ofActionDispatched(Online)).subscribe(() => this.getUsers());
  }

  /**
   * Dispatch get users actions to store
   */
  getUsers() {
    this.store.dispatch(new GetUsers());
  }

}
