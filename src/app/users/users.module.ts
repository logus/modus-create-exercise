import {NgModule} from '@angular/core';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {UsersListComponent} from './users-list/users-list.component';
import {UsersRoutingModule} from './users-routing.module';
import {MaterialModule} from '../material/material.module';
import {BrowserModule} from '@angular/platform-browser';
import {NgxContentLoadingModule} from 'ngx-content-loading';

@NgModule({
  declarations: [
    UserDetailComponent,
    UsersListComponent,
  ],
  entryComponents: [
    UserDetailComponent,
    UsersListComponent,
  ],
  imports: [
    BrowserModule,
    UsersRoutingModule,
    MaterialModule,
    NgxContentLoadingModule
  ]
})

export class UsersModule {

}

