import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {UsersListComponent} from './users-list/users-list.component';
import {UsersGuard} from './guards/users.guard';

const routes: Routes = [
  {
    path: 'users',
    canActivate: [UsersGuard],
    component: UsersListComponent
  },
  {
    path: 'users/:id',
    canActivate: [UsersGuard],
    component: UserDetailComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [UsersGuard]
})
export class UsersRoutingModule {
}
